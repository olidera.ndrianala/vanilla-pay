## About
This is a none official PHP extension library that wraps Vanilla Pay payment methods including : Airtel Money, Mvola, Orange Money and Vanilla Pay in Madagascar. It was developped to resolve issues for more recent PHP application version from official source code.

Official doc and code implementations provided by Vanilla Pay for some php framework and CMS can be download here : https://drive.google.com/drive/folders/1kVZtSQ_qN56RY8wp39LN7tPx_huIcDis .

For more use cases, please visit : https://online.mg/index.php 

## Prerequisites
An account is needed to obtain keys. https://pro.ariarynet.com/login 

## Installation
```
composer require olidera/vanilla-pay
```

## How to use

```php
$publicKey = "your_public_key"; // Provided by Vanilla pay after subscription
$privateKey = "your_private_key"; // Provided by Vanilla pay after subscription
$clientId = "your_client_id"; // Provided by Vanilla pay after subscription
$clientSecret = "your_client_secret"; // Provided by Vanilla pay after subscription
$websiteURL = "your_website_url"; // Your website URL registered in Vanilla Pay account

/** @var EncryptUtils */
$encryptUtils = new EncryptUtils();

/** @var APIUtils */
$apiUtils = new APIUtils();

// Initiate payment
$paymentGateway = new PaymentGateway($publicKey, $privateKey, $clientId, $clientSecret,  $websiteURL, $apiUtils, $encryptUtils);
$paymentProcessor = new PaymentProcessor($paymentGateway);
// Call payment process with needed parameters
$cartId = "000001";
$amount = 500;
$payerName = "Olidera Ndrianala";
$paymentReference = "REF_PAY_000001";
$payerIPAddress = "127.0.0.1"; // Use method to locate current client IP 

// Send informations to Vanilla Pay API and process payment
// If everything goes well, the user will be redirected to Vanilla Pay website to process his payment
$paymentProcessor->process($cartId, $amount, $payerName, $paymentReference, $payerIPAddress);
```

## Tests
```
vendor/bin/phpunit tests
```