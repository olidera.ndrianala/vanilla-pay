<?php

namespace Olidera\VanillaPay\Config;

class Constants
{
    const AUTH_ENDPOINT = 'https://pro.ariarynet.com/oauth/v2/token';
    const PAYMENT_ENDPOINT = 'https://pro.ariarynet.com/api/paiements';
    const PROCESS_PAYMENT_ENDPOINT = 'https://moncompte.ariarynet.com/payer/';
}
