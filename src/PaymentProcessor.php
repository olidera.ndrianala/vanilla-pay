<?php
namespace Olidera\VanillaPay;

use Olidera\VanillaPay\Config\Constants;
use Olidera\VanillaPay\Services\PaymentGatewayInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class PaymentProcessor
{

    private $paymentGateway;
    
	public function __construct(PaymentGatewayInterface $paymentGateway)
    {
        $this->paymentGateway = $paymentGateway;
    }

	/**
     * Process payment : send product and payer description to Vanilla Pay API
     * 
     * @param string $cartId : cart id that is to generate by your app
     * @param float $amount : Amount to pay
     * @param string $nom : payer name
     * @param string $reference : payment reference that is to generate by your app
     * @param string $payerIpAdress
     * @return bool|int|string
     */
    public function process(
        string $cartId, float $amount, string $payerName, 
        string $paymentReference, string $payerIpAddress,
        string $currency = "Ar"
    ) :Response
    {
        $now = new \DateTime();
        $params=array(
            "unitemonetaire" => $currency,
            "adresseip" => $payerIpAddress,
            "date" => $now->format('Y-m-d H:i:s'),
            "idpanier" => $cartId,
            "montant" => $amount,
            "nom" => $payerName,
            "reference" => $paymentReference
        );

        $vanillaPaymentId = $this->paymentGateway->send(Constants::PAYMENT_ENDPOINT, $params);

        return new RedirectResponse(Constants::PROCESS_PAYMENT_ENDPOINT . $vanillaPaymentId);
    }
}