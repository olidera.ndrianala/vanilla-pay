<?php

namespace Olidera\VanillaPay\Services;

/**
 * Interact with payment API
 */
interface PaymentGatewayInterface
{
    /**
     * getAccessToken
     *
     * @return string
     */
    public function getAccessToken() :string|null;

    /**
     * send
     *
     * @param  mixed $endpoint
     * @param  mixed $parameterToSend
     * @return bool
     */
    public function send(string $endpoint, array $parameterToSend) : bool|int|string;

}
