<?php

namespace Olidera\VanillaPay\Services;

use Olidera\VanillaPay\Config\Constants;
use Olidera\VanillaPay\Exception\APIException;
use Olidera\VanillaPay\Exception\AuthenticationException;
use Olidera\VanillaPay\Utils\APIUtilsInterface;
use Olidera\VanillaPay\Utils\EncryptUtilsInterface;

/**
 * PaymentGateway implements methods to communicate with external API
 */
class PaymentGateway implements PaymentGatewayInterface
{
    
    private string $publicKey; // Key received after registering into https://pro.ariarynet.com
    private string $privateKey; // Key received after registering into https://pro.ariarynet.com
    private $clientId; //Generated in VanillaPay account registration https://pro.ariarynet.com
    private $clientSecret; //Generated in VanillaPay account registration https://pro.ariarynet.com
    private string|null $token = null; // The token received from API
    private string $websiteURL; //Website URL regeister in https://pro.ariarynet.com 
    private APIUtilsInterface $apiUtils;
    private EncryptUtilsInterface $encryptUtils;

    public function __construct(
        $publicKey, $privateKey, $clientId, $clientSecret, $websiteURL, 
        APIUtilsInterface $apiUtils, EncryptUtilsInterface $encryptUtils
    ) 
    {
        $this->publicKey = substr($publicKey, 0, 24);
        $this->privateKey = substr($privateKey, 0, 24);
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->websiteURL = $websiteURL;
        $this->apiUtils = $apiUtils;
        $this->encryptUtils = $encryptUtils;
    }

    /**
     * getAccessToken
     *
     * @return string
     */
    public function getAccessToken() :string|null
    {
        if ($this->token!=null) return $this->token;

        $param = [
            'client_id'=> $this->clientId,
            'client_secret'=> $this->clientSecret,
            'grant_type' => 'client_credentials' // Value specified by Vanilla Pay
        ];

        $json= json_decode($this->apiUtils->sendRequest(Constants::AUTH_ENDPOINT, "POST", array(), $param));

        if(isset($json->error)){
            throw new AuthenticationException($json->error.": ".$json->error_description);
        }
        
        $this->token = $json->access_token;

        return $json->access_token;
    }

        
    /**
     * sendRequest
     *
     * @return bool
     */
    public function send(string $endpoint, array $parametersToSend) : bool|int|string
    {
        $encryptedParam = $this->encryptUtils->encrypt($this->publicKey, json_encode($parametersToSend));

        $params = [
            "site_url" => $this->websiteURL,
            "params" => $encryptedParam
        ];
       
        $headers= [
            "Authorization:Bearer ". $this->getAccessToken()
        ];
        
        $json = $this->apiUtils->sendRequest($endpoint,"POST",$headers, $params);
        $error = json_decode($json);
        
        if(isset($error->error)) {
            throw new APIException($error->error.": ".$error->error_description);
        }

        return $this->encryptUtils->decrypt($this->privateKey, $json);
    }
    

}
