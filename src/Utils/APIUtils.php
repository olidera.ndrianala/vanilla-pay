<?php
namespace Olidera\VanillaPay\Utils;

use Exception;

class APIUtils implements APIUtilsInterface
{
    /**
     * Make a request call to endpoint target
     *
     * @param  string $url
     * @param  string $type
     * @param  array $headers
     * @param  mixed $params
     * 
     * @return string
     */
    public function sendRequest(string $url, string $type, array $headers, mixed $params) :string
    {
        $curl = curl_init();
		curl_setopt($curl,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);

        if(strtoupper($type) == 'POST'){
            curl_setopt($curl,CURLOPT_POST,1);
            curl_setopt($curl,CURLOPT_POSTFIELDS,$params);
        }else{
            $query=http_build_query($params);
            $url .= "?".$query;
        }

        curl_setopt($curl,CURLOPT_URL,$url);
	    curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true);
        
        try {
            $result = curl_exec($curl);
            curl_close($curl);
        } catch(Exception $e) {
            throw new Exception("Error playing request : ".$e->getMessage());
        }
        
        return $result;
    }
}