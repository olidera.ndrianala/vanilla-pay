<?php
namespace Olidera\VanillaPay\Utils;

use phpseclib3\Crypt\TripleDES;

/**
 * EncryptUtils : Encrypting data utils
 */
class EncryptUtils implements EncryptUtilsInterface
{
    private TripleDES $encrypter; // Vanilla Pay encrypting data utilities
    
    /**
     * @param  string $encryptMode 
     * @param  string $iv 
     * @return void
     */
    public function __construct(string $encryptMode = "cbc", string $iv="\x00\x00\x00\x00\x00\x00\x00\x00")
    {
        $this->encrypter = new TripleDES($encryptMode);
        $this->encrypter->setIV($iv);
    }

    /**
     * crypter helper : encrypt data with public key provided Vanilla account 
     *
     * @param  string $key : vanilla pay public key
     * @param  string $dataToEncrypt : data to encrypt
     * @return string : Encrypted data
     */
    public function encrypt(string $key, string $dataToEncrypt) :string
    {
        $this->encrypter->setKey($key);

        return $this->encrypter->encrypt($dataToEncrypt);
    }

    /**
     * decrypter helper : decrypt data with private key provided Vanilla Pay account
     *
     * @param  string $key
     * @param  string $data
     * @return string : Decrypted data
     */
    public function decrypt(string $key, string $dataToDecrypt) : string
    {
        $this->encrypter->setKey($key);

        return $this->encrypter->decrypt($dataToDecrypt);
    }
}