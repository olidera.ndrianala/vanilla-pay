<?php
namespace Olidera\VanillaPay\Utils;

interface APIUtilsInterface
{
    /**
     * Make a request call to endpoint target
     *
     *
     * @param  mixed $endpoint
     * @param  mixed $type
     * @param  mixed $headers
     * @param  mixed $params
     * @return string
     */
    public function sendRequest(string $endpoint, string $type, array $headers, mixed $params) :string;
}