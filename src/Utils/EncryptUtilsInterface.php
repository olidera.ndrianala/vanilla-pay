<?php
namespace Olidera\VanillaPay\Utils;

interface EncryptUtilsInterface
{


    /**
     * crypter helper : encrypt data 
     *
     * @param  string $key : vanilla pay public key
     * @param  string $dataToEncrypt : data to encrypt
     * @return string : Encrypted data
     */
    public function encrypt(string $key, string $dataToEncrypt) :string ;

    /**
     * decrypter helper : decrypt data 
     *
     * @param  string $key
     * @param  string $data
     * @return string : Decrypted data
     */
    public function decrypt(string $key, string $dataToDecrypt) :string;
}