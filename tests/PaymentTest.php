<?php
namespace Olidera\VanillaPay\Tests;

use Olidera\VanillaPay\PaymentProcessor;
use Olidera\VanillaPay\Services\PaymentGateway;
use Olidera\VanillaPay\Utils\APIUtils;
use Olidera\VanillaPay\Utils\EncryptUtils;

class PaymentTest extends CommonTestCase
{

    public function testProcessPayment()
    {
        $publicKey = $_ENV['VANILLA_PAY_PUBLIC_KEY'];
        $privateKey = $_ENV['VANILLA_PAY_PRIVATE_KEY'];
        $clientId = $_ENV['VANILLA_PAY_CLIENT_ID'];
        $clientSecret = $_ENV['VANILLA_PAY_CLIENT_SECRET'];
        $websiteURL = $_ENV['VANILLA_PAY_WEBSITE_URL'];

        /** @var EncryptUtils */
        $encryptUtils = new EncryptUtils();
        
        /** @var APIUtils */
        $apiUtils = new APIUtils();

        // Initiate payment
        $paymentGateway = new PaymentGateway($publicKey, $privateKey, $clientId, $clientSecret, $websiteURL, $apiUtils, $encryptUtils);
        $paymentProcessor = new PaymentProcessor($paymentGateway);
        // Call payment process with needed parameters
        $cartId = "000001";
        $amount = 500;
        $payerName = "Olidera Ndrianala";
        $paymentReference = "REF_PAY_000001";
        $payerIPAddress = "127.0.0.1"; // Use method to locate current client IP 

        $response = $paymentProcessor->process($cartId, $amount, $payerName, $paymentReference, $payerIPAddress);

        // In success case : $response must return Response object
        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\Response::class, $response);
    }
}
