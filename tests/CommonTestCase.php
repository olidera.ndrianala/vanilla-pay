<?php

namespace Olidera\VanillaPay\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Dotenv\Dotenv;

class CommonTestCase extends TestCase 
{

    public function setUp(): void
    {
        parent::setUp();

        // Charger les constantes depuis le fichier .env.test
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../.env.test');
    }

}