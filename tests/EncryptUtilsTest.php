<?php

namespace Olidera\VanillaPay\Tests;

use Olidera\VanillaPay\Utils\EncryptUtils;

class EncryptUtilsTest extends CommonTestCase
{

    public function testEncrypt()
    {
        $publicKey = substr($_ENV['VANILLA_PAY_PUBLIC_KEY'], 0, 24);
        $data = json_encode(['key1' => 'value1']);
        $encryptUtils = new EncryptUtils();
        // Encrypt data
        $encryptedData = $encryptUtils->encrypt($publicKey, $data);

        $this->assertIsString($encryptedData);
    }

}
